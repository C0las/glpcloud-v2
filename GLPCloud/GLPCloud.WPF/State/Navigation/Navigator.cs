﻿using GLPCloud.WPF.Commands;
using GLPCloud.WPF.Models;
using GLPCloud.WPF.ViewModels;
using System.Windows.Input;

namespace GLPCloud.WPF.State.Navigation
{
    public class Navigator : ObservableObject, INavigator
    {
        private ViewModelBase _currentViewModel;

        public ViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel = value;
                OnPropertyChanged(nameof(CurrentViewModel));
            }
        }

        public ICommand UpdateCurrentViewModelCommand => new UpdateCurrentViewModelCommand(this);
    }
}