﻿using GLPCloud.WPF.ViewModels;
using System.Windows.Input;

namespace GLPCloud.WPF.State.Navigation
{
    public enum ViewType
    {
        Home,
        Performance,
        Reports
    }

    public enum ActionBar
    {
        Exit,
        Maximize,
        Minimize
    }

    public interface INavigator
    {
        public ViewModelBase CurrentViewModel { get; set; }
        public ICommand UpdateCurrentViewModelCommand { get; }
    }
}