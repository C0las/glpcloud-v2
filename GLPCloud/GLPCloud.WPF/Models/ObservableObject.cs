﻿using System.ComponentModel;

namespace GLPCloud.WPF.Models
{
    /// <summary>
    /// Un modelo de vista base que dispara eventos de cambio de propiedad según sea necesario
    /// </summary>
    public class ObservableObject : INotifyPropertyChanged
    {
        /// <summary>
        /// El evento que se dispara cuando cualquier propiedad ajena cambia su valor
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Se llama este metodo para disparar un eveneto <see cref="PropertyChanged"/>
        /// </summary>
        /// <param name="propertyName"></param>
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}