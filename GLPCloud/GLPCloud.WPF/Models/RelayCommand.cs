﻿using System;
using System.Windows.Input;

namespace GLPCloud.WPF.Models
{
    public class RelayCommand : ICommand
    {
        #region Private Members

        /// <summary>
        /// Accion de ejecucion
        /// </summary>
        private Action mAction;

        #endregion Private Members

        #region Public Events

        /// <summary>
        /// El evento que se dispara cuando el valor de <see cref="CanExecute(object)"/> ha cambiado
        /// </summary>
        public event EventHandler CanExecuteChanged = (sender, e) => { };

        #endregion Public Events

        #region Constructor

        public RelayCommand(Action action)
        {
            mAction = action;
        }

        #endregion Constructor

        #region Command Methods

        /// <summary>
        /// Un comando de relevo siempre puede ejecutar
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns>True</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Ejecuta los comandos Action
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            mAction();
        }

        #endregion Command Methods
    }
}