﻿using System.Windows;
using System.Windows.Input;
using GLPCloud.WPF.State.Navigation;

namespace GLPCloud.WPF.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public INavigator Navigator { get; set; } = new Navigator();

        public MainViewModel()
        {
            Navigator.UpdateCurrentViewModelCommand.Execute(ViewType.Home);
        }
    }
}