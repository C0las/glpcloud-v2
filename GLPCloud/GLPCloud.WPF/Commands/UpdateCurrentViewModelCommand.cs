﻿using GLPCloud.WPF.State.Navigation;
using GLPCloud.WPF.ViewModels;
using System;
using System.Windows;
using System.Windows.Input;

namespace GLPCloud.WPF.Commands
{
    public class UpdateCurrentViewModelCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly INavigator _navigator;

        public UpdateCurrentViewModelCommand(INavigator navigator)
        {
            _navigator = navigator;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (!(parameter is ViewType viewType)) return;
            _navigator.CurrentViewModel = viewType switch
            {
                ViewType.Home => new HomeViewModel(),
                ViewType.Performance => new PerformanceViewModel(),
                ViewType.Reports => new ReportViewModel(),
                _ => _navigator.CurrentViewModel
            };

            if (parameter is ActionBar actionBar)
            {
                switch (actionBar)
                {
                    case ActionBar.Exit:
                        Application.Current.Shutdown();
                        break;
                }
            }
        }
    }
}