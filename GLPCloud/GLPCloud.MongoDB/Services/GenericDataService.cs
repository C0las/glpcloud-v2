﻿using GLPCloud.Domain.Models;
using GLPCloud.Domain.Services;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GLPCloud.MongoDB.Services
{
    public class GenericDataService<T> : IDataService<T> where T : DomainObject
    {
        private readonly IMongoDatabase _db;

        /// <summary>
        /// Constructor inicializa conexion a la Base de Datos
        /// </summary>
        public GenericDataService()
        {
            var client = new MongoClient("mongodb+srv://nicolash:rut197758449@cluster0-1scdm.gcp.mongodb.net/glpCloud?retryWrites=true&w=majority");
            _db = client.GetDatabase("glpCloud");
        }

        /// <summary>
        /// Obtiene el nombre de la coleccion
        /// </summary>
        /// <returns>Una Coleccion</returns>
        private IMongoCollection<T> Collection()
        {
            return _db.GetCollection<T>(typeof(T).Name);
        }

        /// <summary>
        /// Inserta una coleccion en la BD
        /// </summary>
        /// <param name="entity">Coleccion</param>
        /// <returns>True or False</returns>
        public async Task<bool> Create(T entity)
        {
            try
            {
                await Collection().InsertOneAsync(entity);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Elimina una coleccion de la BD
        /// </summary>
        /// <param name="id">Indentificador de la coleccion</param>
        /// <returns>True or False</returns>
        public async Task<bool> Delete(ObjectId id)
        {
            try
            {
                var filter = Builders<T>.Filter.Eq("Id", id);
                await Collection().DeleteOneAsync(filter);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Obtiene una coleccion de la BD mediante el indentificador
        /// </summary>
        /// <param name="id">Indentificador de la coleccion</param>
        /// <returns>Una coleccion</returns>
        public async Task<T> Get(ObjectId id)
        {
            var filter = Builders<T>.Filter.Eq("Id", id);
            var entity = await Collection().FindAsync(filter).Result.FirstAsync();
            return entity;
        }

        /// <summary>
        /// Obtiene todas las colecciones de la BD
        /// </summary>
        /// <returns>Arreglo de Colecciones</returns>
        public async Task<IEnumerable<T>> GetAll()
        {
            var entities = await Collection().FindAsync(new BsonDocument()).Result.ToListAsync();
            return entities;
        }

        /// <summary>
        /// Actualiza una coleccion de la BD
        /// </summary>
        /// <param name="id">Indentidicador de la coleccion</param>
        /// <param name="entity">Coleccion a actualizar</param>
        /// <returns>True or False</returns>
        public async Task<bool> Update(ObjectId id, T entity)
        {
            try
            {
                await Collection().ReplaceOneAsync(new BsonDocument("_id", id), entity);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}