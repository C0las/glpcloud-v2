﻿using GLPCloud.Domain.Models;
using GLPCloud.Domain.Services;
using GLPCloud.MongoDB.Services;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.IdGenerators;
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            IDataService<Performance> data = new GenericDataService<Performance>();
            var result = data.Get(new ObjectId("5f1653ad872a9f2c74ef13e1")).Result;
            result.Capacity = "15";
            Console.WriteLine(data.Update(result.Id, result).Result);
            
            Console.ReadLine();
            
        }
    }
}
