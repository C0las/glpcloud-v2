﻿using MongoDB.Bson;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GLPCloud.Domain.Services
{
    public interface IDataService<T>
    {
        /// <summary>
        /// Retorna todos los elementos
        /// </summary>
        /// <returns>Una Coleccion</returns>
        Task<IEnumerable<T>> GetAll();

        /// <summary>
        /// Retorna un elemento por el Id
        /// </summary>
        /// <param name="id">identificador del elemento</param>
        /// <returns>documento</returns>
        Task<T> Get(ObjectId id);

        /// <summary>
        /// Crea un elemento
        /// </summary>
        /// <param name="entity">Documento</param>
        /// <returns>Un documento</returns>
        Task<bool> Create(T entity);

        /// <summary>
        /// Actualiza un elemento
        /// </summary>
        /// <param name="id">Identifiacor del elemento</param>
        /// <param name="entity">Documento</param>
        /// <returns>Un Documento</returns>
        Task<bool> Update(ObjectId id, T entity);

        /// <summary>
        /// Elimina un elemento
        /// </summary>
        /// <param name="id">Identificador del elemento</param>
        /// <returns>Un documento</returns>
        Task<bool> Delete(ObjectId id);
    }
}