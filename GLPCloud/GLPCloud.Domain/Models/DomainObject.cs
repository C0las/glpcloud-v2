﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GLPCloud.Domain.Models
{
    public class DomainObject
    {
        /// <summary>
        /// Propiedad identificador del elemento
        /// </summary>
        [BsonId]
        public ObjectId Id { get; set; }
    }
}