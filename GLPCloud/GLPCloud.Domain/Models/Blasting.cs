﻿namespace GLPCloud.Domain.Models
{
    public class Blasting
    {
        /// <summary>
        /// Propiedad cilindros pintados
        /// </summary>
        public int Painted { get; set; }

        /// <summary>
        /// Propiedad cilindros granallados acomulados
        /// </summary>
        public int Acomulated { get; set; }

        /// <summary>
        /// Propiedad cilindros granallados rechazados
        /// </summary>
        public int Rejected { get; set; }

        /// <summary>
        /// Propiedad total de cilindros granallados
        /// </summary>
        public int TotalBlasting { get; set; }
    }
}