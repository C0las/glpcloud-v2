﻿namespace GLPCloud.Domain.Models
{
    public class Hydraulic
    {
        /// <summary>
        /// Propiedad cilindros hidraulicos acomulados
        /// </summary>
        public int Acomulated { get; set; }

        /// <summary>
        /// Propiedad cilindros hidraulicos rechazados
        /// </summary>
        public int Rejected { get; set; }

        /// <summary>
        /// Propiedad total de cilindros hidraulicos
        /// </summary>
        public int TotalHydraulic { get; set; }
    }
}