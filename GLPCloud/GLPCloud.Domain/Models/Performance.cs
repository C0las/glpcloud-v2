﻿using System;

namespace GLPCloud.Domain.Models
{
    public class Performance : DomainObject
    {
        /// <summary>
        /// Propiedad fecha del rendimiento
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Propiedad capacidad del cilindro
        /// </summary>
        public string Capacity { get; set; }

        /// <summary>
        /// Propiedad cilindros granallados
        /// </summary>
        public Blasting Blasting { get; set; }

        /// <summary>
        /// Propiedad cilindros hidraulicos
        /// </summary>
        public Hydraulic Hydraulic { get; set; }
    }
}